# ================== LOOK HERE :) ==================
# If you're writing a .csv file, you'll want to `import csv`
import csv
import re
from pathlib import Path
from typing import List, Optional

from bs4 import BeautifulSoup

import util
from util import LD, Cell, LDFile

INPUT_PATH = Path('..','input')
#INPUT_PATH = Path('..', 'all_exb')
EXB_FILES = sorted(util.exb_in(INPUT_PATH))

REFERENTS_WITH_LD = ['car1', 'man', 'woman2', 'driver1', 'car2',
                     'ball', 'couple', 'cars', 'drivers', 'woman1', 'family']
ld_general_dict = dict()

def create_ld(referent: Cell, intervening_tier: List[Cell], direction_tier: List[Cell],
              np_tier: List[Cell], pronoun_tier: List[Cell], function_tier: List[Cell],
              cu_tier: List[Cell], startId: str, endId: str, ld_flag: bool, part_flag:str) -> Optional[LD]:
    try:
        intervening = util.cells_intersecting_with_cell(referent, intervening_tier)[0]
        direction = util.cells_intersecting_with_cell(referent, direction_tier)[0]
        np = util.cells_intersecting_with_cell(referent, np_tier)[0]
        pronoun = util.cells_intersecting_with_cell(referent, pronoun_tier)[0]
        function = util.cells_intersecting_with_cell(referent, function_tier)[0]
    except IndexError:
        print('NOT ALL TIERS INTERSECT ================================')
        return None
        
    cu_cells = util.cells_surrounding_cells(referent, cu_tier)
    cu = ' '.join(cu.value for cu in cu_cells)
    previous_cu_cell = cu_cells[0].get_previous(cu_tier)
    previous_cu = previous_cu_cell.value if previous_cu_cell else None
    next_cu_cell = cu_cells[-1].get_next(cu_tier)
    next_cu = next_cu_cell.value if next_cu_cell else None
    id_span = startId+'-'+endId

    return LD(referent.start, referent.end,
              referent, direction,
              np, pronoun, intervening, function,
              previous_cu, cu, next_cu, id_span, ld_flag, part_flag)


class RefFile:

    path: str
    cu_count: int
    all_referents_count: int
    selected_referents_count: int

    def __init__(self, path, cu_count, all_referents_count, selected_referents_count):
        self.path = path
        self.cu_count = cu_count
        self.all_referents_count = all_referents_count
        self.selected_referents_count = selected_referents_count


def create_ref_file(path: Path, soup: BeautifulSoup):
    if not util.has_tier(soup, 'referent') and util.has_tier(soup, 'dipl'):
        return

    #cu_tier = util.cells_in_tier(soup, 'cu')
    all_referents_tier = util.cells_in_tier(soup, 'referent')

    selected_referents = [ref for ref in all_referents_tier
                          if ref.value in REFERENTS_WITH_LD]

    #cus_with_text = [cu for cu in cu_tier
    #                 if re.search(r"[a-zA-Z](?![^{(\[]*[})\]])", cu.value)]

    #cu_count = len(cus_with_text)
    cu_count = 0
    all_referents_count = len(all_referents_tier)
    selected_referents_count = len(selected_referents)

    return RefFile(path, cu_count, all_referents_count, selected_referents_count)


def create_ld_file(path: Path, soup: BeautifulSoup, isFileValid: bool) -> Optional[LDFile]:

    if(isFileValid):
        all_referents_tier = util.cells_in_tier(soup, 'referent')
        narrative_tier = util.cells_in_tier(soup, 'dipl')
        narrative = ' '.join([cell.value for cell in narrative_tier])

        listnew = list()
        global ld_general_dict
        duplicate_ref = dict()
                    
        #below if code runs if the file contains ld related tiers and in turn ld referents
        if (util.has_tier(soup, 'ld_intervening')
                and util.has_tier(soup, 'ld_referent')
                and util.has_tier(soup, 'ld_direction')
                and util.has_tier(soup, 'ld_np')
                and util.has_tier(soup, 'ld_pronoun')
                and util.has_tier(soup, 'ld_function')
                and util.has_tier(soup, 'cu')
                and util.has_tier(soup, 'dipl')):
            #return
    
            intervening_tier = util.cells_in_tier(soup, 'ld_intervening')
            referent_tier = util.cells_in_tier(soup, 'ld_referent')
            direction_tier = util.cells_in_tier(soup, 'ld_direction')
            np_tier = util.cells_in_tier(soup, 'ld_np')
            pronoun_tier = util.cells_in_tier(soup, 'ld_pronoun')
            cu_tier = util.cells_in_tier(soup, 'cu')
            ld_referent = util.cells_in_tier(soup, 'ld_referent')
            function_tier = util.cells_in_tier(soup, 'ld_function')
        
            # Add tier text
            for cell in np_tier:
                cell.text = ' '.join([c.value for c in util.cells_intersecting_with_cell(cell, narrative_tier)])
            for cell in direction_tier:
                cell.text = ' '.join([c.value for c in util.cells_intersecting_with_cell(cell, narrative_tier)])
            for cell in referent_tier:
                cell.text = ' '.join([c.value for c in util.cells_intersecting_with_cell(cell, narrative_tier)])
            for cell in pronoun_tier:
                cell.text = ' '.join([c.value for c in util.cells_intersecting_with_cell(cell, narrative_tier)])
            for cell in intervening_tier:
                cell.text = ' '.join([c.value for c in util.cells_intersecting_with_cell(cell, narrative_tier)])
            
            #code for LD referents
            if referent_tier:
                for referent_cell in referent_tier:
                    id1 = str(referent_cell.start)
                    id2 = str(referent_cell.end-1)
                    ld_flag=1
                    part_flag = '1'
                    lds = [create_ld(referent_cell,
                    intervening_tier, direction_tier,
                    np_tier, pronoun_tier, function_tier, cu_tier, id1, id2, ld_flag, part_flag)]
                    duplicate_ref[id1] = referent_cell.value
                    listnew.append(lds)
                    ld_general_dict = create_ld_general_column(ld_general_dict, referent_cell.value, ld_flag)
            #code for non LD referents
            for referent_cell in all_referents_tier:
                id1 = str(referent_cell.start)
                id2 = str(referent_cell.end-1)
                ld_flag = 0
                part_flag = '1'
                if duplicate_ref and id1 in duplicate_ref.keys() and duplicate_ref[id1] == referent_cell.value:
                    continue
                else:                    
                    if util.cells_within_cell(referent_cell, pronoun_tier) or util.cells_within_cell(referent_cell, np_tier):
                        part_flag = '2'
    
                    lds = [display_nonLD(soup, referent_cell, ld_flag, part_flag)]
                    listnew.append(lds)
                    ld_general_dict = create_ld_general_column(ld_general_dict, referent_cell.value, ld_flag)
            
            selected_referents = [ref for ref in all_referents_tier
                              if ref.value in REFERENTS_WITH_LD]
            cus_with_text = [cu for cu in cu_tier
                             if re.search(r"[a-zA-Z](?![^{(\[]*[})\]])", cu.value)]
            cu_count = len(cus_with_text)
            ld_count = len(lds)
            all_referents_count = len(all_referents_tier)
            selected_referents_count = len(selected_referents)
        
        #below else condition is executed if the file does not contain LD tiers
        else:
            for referent_cell in all_referents_tier:
                id1 = str(referent_cell.start)
                id2 = str(referent_cell.end-1)
                ld_flag = 0
                part_flag = '1'
                lds = [display_nonLD(soup, referent_cell, ld_flag, part_flag)]
                listnew.append(lds)
            
                ld_general_dict = create_ld_general_column(ld_general_dict, referent_cell.value, ld_flag)
            cu_count = 0
            ld_count = 0
            selected_referents = [ref for ref in all_referents_tier
                              if ref.value in REFERENTS_WITH_LD]
            all_referents_count = len(all_referents_tier)
            selected_referents_count = len(selected_referents)
        
        if not all(lds):
            print('NOT ALL LDS =============================')
            return None
        
        for keys in ld_general_dict:
            if isinstance(ld_general_dict[keys], list):
                ld_general_dict[keys] = any(flatten_lists(ld_general_dict[keys]))
    
        return LDFile(path, narrative, listnew, cu_count, ld_count, all_referents_count, selected_referents_count)
    
    else:
        print("Invalid file- ", path)


def checkFileValid(path, soup):
    try:
        all_ref = util.cells_in_tier(soup, 'referent')
        cu = util.cells_in_tier(soup, 'cu') if util.has_tier(soup, 'cu') else util.cells_in_tier(soup, 'CU')
        if util.has_tier(soup, 'ld_intervening'):
            ld_inter = util.cells_in_tier(soup, 'ld_intervening')
        if (not(all_ref) or not(cu)):
            return False
        else:
            return True
    except AttributeError:
        return False

#below function is called to display non LD referents in a totally non LD file
def display_nonLD(soup, referent_cell, ld_flag, part_flag) -> Optional[LD]:
    id1 = str(referent_cell.start)
    id2 = str(referent_cell.end-1)
    id_span = id1+'-'+id2
    intervening_tier1 = None
    direction_tier1 = None
    np_tier1 = None
    pronoun_tier1 = None
    function_tier1 = None
    try:
        cu_tier = util.cells_in_tier(soup, 'cu') or util.cells_in_tier(soup, 'CU')
        cu_cells = util.cells_surrounding_cells(referent_cell, cu_tier)
        cu = ' '.join(cu.value for cu in cu_cells)
        previous_cu_cell = cu_cells[0].get_previous(cu_tier) if cu_cells else None
        previous_cu = previous_cu_cell.value if previous_cu_cell else None
        next_cu_cell = cu_cells[-1].get_next(cu_tier) if cu_cells else None
        next_cu = next_cu_cell.value if next_cu_cell else None
    except AttributeError:
        cu_tier = None
        previous_cu = None
        cu = None
        next_cu = None
    
    return LD(referent_cell.start, referent_cell.end,
              referent_cell, direction_tier1,
              np_tier1, pronoun_tier1, intervening_tier1, function_tier1,
              previous_cu, cu, next_cu, id_span, ld_flag, part_flag)

def flatten_lists(list_of_values):
    if len(list_of_values) == 0:
        return list_of_values
    if isinstance(list_of_values[0], list):
            return flatten_lists(list_of_values[0])+ flatten_lists(list_of_values[1:])
    return list_of_values[:1] + flatten_lists(list_of_values[1:])
    
    
def create_ld_general_column(dict_obj, key, value):
    if key not in dict_obj:
        dict_obj[key] = value
    else:
        dict_obj[key] = [dict_obj[key],value]
    return dict_obj

def write_ref_file_csv(ld_files: List[LDFile]):
    with open('referent_counts.csv', 'w', newline='', encoding='utf-8') as csvfile:
        fieldnames = ['file_name', 'cu_count', 'all_referents_count', 'selected_referents_count','all_other_referents']

        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for ld_file in ld_files:
            fields = {'file_name': ld_file.path.name,
                      'cu_count': ld_file.cu_count,
                      'all_referents_count': ld_file.all_referents_count,
                      'selected_referents_count': ld_file.selected_referents_count}
            writer.writerow(fields)


# ============= LOOK HERE :) ==============
# This function writes the .csv file.
def write_ld_file_csv(ld_files: List[LDFile], invalid_files):
    with open('output_dataframe.csv', 'w', newline='', encoding='utf-8') as csvfile:
        fieldnames = ['file_name',
                      'referent','ld_in_this_file','ld_in_general','id_span','ld_part', 'direction', 'np', 'pronoun', 'intervening',
                      'function','ld_text', 'verb_phrase_text',
                      'np_text', 'pronoun_text', 'intervening_text',
                      'previous_cu', 'cu', 'next_cu', 'narrative',
                      'ld_count', 'cu_count', 'all_referents_count', 'selected_referents_count']

        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for inv in invalid_files:
            fields = {'file_name': inv.name,
                      'referent': 'No referents found'}
            writer.writerow(fields)
        
        for ld_file in ld_files:
            for ld in ld_file.lds:
                for sub_ld in ld:
                    if sub_ld is not None:
                        fields = {'file_name': ld_file.path.name,
                              'referent': sub_ld.referent.value,
                              'id_span': sub_ld.id_span,
                              'ld_in_this_file': 'Yes' if sub_ld.ld_flag==1 else 'No',
                              'ld_in_general' : 'Yes' if ld_general_dict[sub_ld.referent.value]==True else 'No',
                              'ld_part' : sub_ld.part_flag,
                              'direction': sub_ld.direction.value if sub_ld.direction else 'NA',
                              'np': sub_ld.np.value if sub_ld.np else 'NA',
                              'pronoun': sub_ld.pronoun.value if sub_ld.pronoun else 'NA',
                              'intervening': sub_ld.intervening.value if sub_ld.intervening else 'NA',
                              'function' : sub_ld.function.value if sub_ld.function else 'NA',
                              'ld_text': sub_ld.referent.text or 'NA',
                              'verb_phrase_text': sub_ld.direction.text if sub_ld.direction else 'NA',
                              'np_text': sub_ld.np.text if sub_ld.np else 'NA',
                              'pronoun_text': sub_ld.pronoun.text if sub_ld.pronoun else 'NA',
                              'intervening_text': sub_ld.intervening.text if sub_ld.intervening else 'NA',
                              'previous_cu': sub_ld.previous_cu,
                              'cu': sub_ld.cu,
                              'next_cu': sub_ld.next_cu,
                              'narrative': ld_file.narrative,
                              'ld_count': ld_file.ld_count,
                              'cu_count': ld_file.cu_count,
                              'all_referents_count': ld_file.all_referents_count,
                              'selected_referents_count': ld_file.selected_referents_count}
                    writer.writerow(fields)


if __name__ == '__main__':
    ld_files = list()
    invalid_files = list()
    for file_path in EXB_FILES:
        with file_path.open(encoding='utf-8') as file:
            # print(file_path.as_posix())
            file_soup = BeautifulSoup(file, features='html.parser')
            isFileValid = checkFileValid(file_path, file_soup)
            if (isFileValid == False):
                invalid_files.append(file_path)
            ld_file = create_ld_file(file_path, file_soup, isFileValid)
            #ref_file = create_ref_file(file_path, file_soup)
            if ld_file is not None:
                print(file_path.as_posix())
            #if ref_file is not None:
                #print(file_path.as_posix())
        ld_files.append(ld_file)
        #print(len(ld_files))

    #bad_files = [f.path for f in ld_files if f is None]
    #print('\n'.join(bad_files))
    ld_files = [f for f in ld_files if f is not None]
    write_ld_file_csv(ld_files, invalid_files)
    
    #ref_files = [f for f in ref_files if f is not None]
    #write_ref_file_csv(ref_files)
    #write_ref_file_csv(ld_files)