from argparse import ArgumentParser
from collections import defaultdict
import glob
import json
import logging
import os
from xml.etree import ElementTree
import csv


_DESCRIPTION = '''This script injects annotations into EXMARaLDA files based on conditions
 provided in an external file.'''

_logger = logging.getLogger(__name__)

# those are constants for parsing EXMARaLDA xml
_EXB_ATTR_CATEGORY = 'category'
_EXB_ATTR_END = 'end'
_EXB_ATTR_ID = 'id'
_EXB_ATTR_SPEAKER = 'speaker'
_EXB_ATTR_START = 'start'
_EXB_ATTR_TIME = 'time'
_EXB_ATTR_TYPE = 'type'
_EXB_TAG_BASIC_BODY = 'basic-body'
_EXB_TAG_COMMON_TIMELINE = 'common-timeline'
_EXB_TAG_EVENT = 'event'
_EXB_TAG_TIER = 'tier'
_EXB_TAG_TLI = 'tli'
CONDITIONAL_TIERS = (('norm','ref-mod1'),('norm','ref-mod2'),('norm','ref-mod3'),('norm','ref-mod4'),('norm','ref-fin'),('norm','ref-clause'),('norm','ref-displ'))
#REF_TIERS = (('norm','ref-form1'),('norm','ref-form2'),('norm','ref-form3'),('norm','ref-form4'),('norm', 'ref-gr'))


def map_to_tier(path,
                context_to_value,
                dry=False,
                overwrite_existing_annotations=False):
    """
    This function checks all annotations defined in the context_to_value map. If all conditions are fulfilled, new
    annotations are created accordingly.
    :param path: Path of EXMARaLDA file.
    :param context_to_value: Mapping configuration.
    :param dry: Only simulate, no write out.
    :param overwrite_existing_annotations: If the target tier already exists do or do not overwrite conflicting
    annotations, i. e. in the same timeslot. Empty timeslots will nonetheless be filled.
    :return:
    """
    _logger.info(f'Starting file {path} ...')
    xml_target, document_map, timeline, none_set = _read_exb(path)    
    
    
    if _logger.isEnabledFor(logging.DEBUG):
        _logger.debug(f'Document map is:{os.linesep}{document_map}')
    for context_definition, target in context_to_value.items():
        matches, index = _collect(context_definition, document_map, none_set) #function which actually checks the condition and returns which timeslots are to be filled
        if _logger.isEnabledFor(logging.DEBUG):
            _logger.debug(f'Matches for {context_definition}:{os.linesep}{matches}')
        new_name_list=list()    
        speaker, new_name, new_value = target
        for i in index:
            new_name_list.append(new_name+str(i))
            
        if len(new_name_list)==0:
            new_name_list.append(new_name)
            
        for name in new_name_list:
            
            existing_annotations = _expand_invervals(document_map[(speaker, name)], timeline)
            if _logger.isEnabledFor(logging.DEBUG):
                _logger.debug(f'Existing annotations for {name} @:{os.linesep}{existing_annotations}')
            conflicts = matches.intersection(existing_annotations)
            if _logger.isEnabledFor(logging.DEBUG):
                _logger.debug(f'Conflicts @:{os.linesep}{conflicts}')
            target_tier = _tier(xml_target,
                                speaker,
                                name)
            if target_tier is None:
                return
            overwrite_existing_annotations = True
            new_annotations = matches if overwrite_existing_annotations else matches.difference(conflicts)
            _insert_new_annotations(target_tier,
                                    timeline,
                                    new_annotations,
                                    new_value)
            if overwrite_existing_annotations:
                _delete_annotations(target_tier, conflicts)
    if not dry:
        xml_target.write(path, encoding='utf-8', xml_declaration=True)


def _collect(query, probe, none_set):
    """
    This function looks up annotations in the document map.
    :param query: A tuple (speaker_name, anno_name, anno_value) of desired annotations. `anno_value` can be None.
    :param probe: The document map to look annotations up in.
    :param none_set : This object contains data of all the empty cells
    :return: Matching timeslots that fulfill all query conditions.
    """
    matches = {ttpl for time_coll in probe.values() for ttpl in time_coll}
    intersections = {ttpl for time_coll in none_set.values() for ttpl in time_coll}
    na_cond_values = ['!svo','!rel','!cop','!cop-rel'] #special case
    final_matches = set()
    probeKeys = probe.keys()
    ref_form_list = list()
    ref_mod_list =list()
    index_list = list()

    #to create a list of all ref-forms and ref-mods
    for k in range(1,5):
        if 'ref-form'+str(k) in probeKeys:
            ref_form_list.append('ref-form'+str(k))
        if 'ref-mod'+str(k) in probeKeys:
            ref_mod_list.append('ref-mod'+str(k))
    
    none_set_checked = False
    for current_rule in query:
        speaker_name = current_rule[0]
        anno_name = current_rule[1]
        anno_value = current_rule[2]
        

        #to check conditions for adding 'simple' tag
        if anno_name in str(ref_mod_list) or anno_name in str(ref_form_list):
            for i in range(len(ref_mod_list)):
                if anno_name in str(ref_mod_list):
                        anno_name = ref_mod_list[i]
                        if anno_value == 'None':
                            intersections &= none_set.get((speaker_name, anno_name))
                            none_set_checked = True
                        else:
                            matches &= probe[(speaker_name, anno_name, anno_value)]  
                
                elif anno_name in str(ref_form_list):
                    anno_name = ref_form_list[i]
                    if anno_value == 'None':
                        matches &= probe[(speaker_name, anno_name)]
                    else:
                        matches &= probe[(speaker_name, anno_name, anno_value)]                   
        
        if anno_value == 'None':
            intersections &= none_set.get((speaker_name, anno_name))
            none_set_checked = True
        elif anno_value == '!None':
            matches &= probe[(speaker_name, anno_name)]
        elif anno_value in na_cond_values:
            matches &= probe[(speaker_name, anno_name)] - probe[(speaker_name, anno_name, anno_value.split('!')[1])]
        else:
            matches &= probe[(speaker_name, anno_name, anno_value)]
           
    if(none_set_checked):
        for match in matches:
            time_range = get_timeline(match)
            time_range.append(match[1])
            all_slots_present =  all(elem in intersections  for elem in time_range) #checking if all timeslots are empty, then only add
            if (all_slots_present):
                final_matches.add(match)
                
    else:
        final_matches = matches
    
    if current_rule[1] == 'ref-form' and bool(matches):
            index_list.append(i+1)

    return final_matches, index_list


def _expand_invervals(intervals, timeline):
    """
    This function expands a list of intervals to all covered one step intervals. This is requried to determine,
    if annotations partially overlap. E. g. an annotation from time 1 to 3 also covers annotations from 1 to 2
    and 2 to 3. This operates on timeline item IDs, not actual times. Therefore `timeline` is required for ordering
    interval limits.
    :param intervals: Given unexpanded intervals.
    :param timeline: Timeline object, a map from timeline item ids to times (floats).
    :return: Expanded interval that is a superset of input parameter `intervals`.
    """
    sorted_tlis = sorted(timeline, key=lambda t: timeline[t])
    expanded_invervals = set()
    expanded_invervals.update(intervals)
    for time_tuple in intervals:
        start_id, end_id = time_tuple
        start_ix = sorted_tlis.index(start_id)
        end_ix = sorted_tlis.index(end_id)
        if start_ix >= end_ix:
            _logger.warning(f'An event has a higher start than end time ({timeline[start_id]}, {timeline[end_id]}).'
                            f' This can cause issues.'
                            f' Interval will be reversed for the new annotation, but not for the matching one.')
            start_ix, end_ix = sorted([start_ix, end_ix])
        all_times = sorted_tlis[start_ix:end_ix + 1]
        if len(all_times) < 2:
            err_msg = f'There was an error computing the correct length of an annotation:' \
                      f' {time_tuple}'
            _logger.error(err_msg)
            if _logger.isEnabledFor(logging.DEBUG):
                _logger.debug(f'Time values: {all_times},'
                              f' indices: {sorted_tlis.index(start_id)}, {sorted_tlis.index(end_id)}')
            raise ValueError
        for start_ix in range(len(all_times) - 1):
            expanded_invervals.add(tuple(all_times[start_ix:start_ix + 1]))
    return expanded_invervals


def _tier(xml_target, speaker, tier_name):
    """
    This function creates or retrieves the target tier of annotations.
    :param xml_target: EXMARaLDA xml object.
    :param speaker: Speaker name of new tier.
    :param tier_name: Tier name of new tier.
    :return: Tier element.
    """
    tier = xml_target.find(f'.//tier[@{_EXB_ATTR_SPEAKER}="{speaker}"][@{_EXB_ATTR_CATEGORY}="{tier_name}"]')
    if tier is None:
        n = len(xml_target.findall(f'.//{_EXB_TAG_TIER}'))
        attributes = dict(category=tier_name,
                          speaker=speaker,
                          id=f'{_EXB_TAG_TIER[:-1]}{n + 2}'.upper(),
                          type='a')
        attributes['display-name'] = f'{speaker} [{tier_name}]'
        tier = ElementTree.SubElement(xml_target.find(f'.//{_EXB_TAG_BASIC_BODY}'), _EXB_TAG_TIER, attributes)
    return tier


def _delete_annotations(tier, intervals):
    """
    This function deletes all annotations defined on the given intervals.
    :param tier: XML tier object.
    :param intervals: Intervals to be deleted. Intervals contain of the exact start and end time.
    :return:
    """
    for start_id, end_id in intervals:
        event = tier.find(f'./{_EXB_TAG_EVENT}[@{_EXB_ATTR_START}="{start_id}"][@{_EXB_ATTR_END}="{end_id}"]')
        tier.remove(event)


def _insert_new_annotations(target_tier,
                            timeline,
                            intervals,
                            value):
    """
    This function inserts annotations with a fixed value for all intervals provided. An interval is a tuple of exact
    start and end time.
    :param target_tier: XML tier object.
    :param timeline: Timeline object for ordering events in the tier.
    :param intervals: Intervals to be annotated with the specific value.
    :param value: Annotation value.
    :return:
    """
    events = [e for e in target_tier]
    for e in events:
        target_tier.remove(e)
    for start_id, end_id in intervals:
        event = ElementTree.Element(_EXB_TAG_EVENT, dict(start=start_id, end=end_id))
        event.text = value
        events.append(event)
    events.sort(key=lambda e: timeline[e.attrib[_EXB_ATTR_START]])
    target_tier.extend(events)

def write_to_csv(data):
    with open('failed_conditions.csv', 'w', newline='', encoding='utf-8') as csvfile:
        col_names = ['file_name', 'speaker', 'tier_name','token_nos.']
        
        writer = csv.DictWriter(csvfile, fieldnames=col_names)
        writer.writeheader()
        
        for row in data:
            for tk in row['token_numbers']:
                fields = {'file_name' : row['file_name'],
                      'speaker' : row['speaker'],
                      'tier_name' : row['tier_name'],
                      'token_nos.' : tk
                }
                writer.writerow(fields)

def check_failed_conditions(path, full_set, none_set):

    """
    This function checks all the conditions under which a particular referent has to be logged into the csv file along with the time interval ID and the respective tier name
    """

    filled_refgr = full_set[('norm', 'ref-gr')]
    filled_refcl = full_set[('norm','ref-clause')] - full_set[('norm','ref-clause','emb')]
    filled_referents = full_set[('norm', 'referent')]
    filled_refembc = full_set[('norm','ref-ss','rel')] | full_set[('norm','ref-ss','rel-p')] | full_set[('norm','ref-ss','rel-e-there')] | full_set[('norm','ref-ss','cop-rel')]
    filled_reffin = full_set[('norm','ref-fin','nf')]
    filled_na = full_set[('norm','ref-ss')]
    filled_ss = full_set[('norm','ref-ss','svo')] | full_set[('norm','ref-ss','rel')] | full_set[('norm','ref-ss','cop')] | full_set[('norm','ref-ss','cop-rel')]
    filled_displ = full_set[('norm','ref-displ')] - full_set[('norm','ref-displ','na')]
    anomalous_data = list()
    anomalies = dict()
    
    emb_cond1 = filled_refgr & filled_refcl & filled_referents & (filled_refembc.union(filled_reffin))#checking if ref-clause is not emb
    emb_cond2 = filled_reffin.difference(filled_referents) 
    emb_cond3 = filled_refembc.difference(filled_referents)
    na_cond = filled_na.symmetric_difference(filled_referents) | filled_na.symmetric_difference(filled_refgr)
    displ_cond = filled_refgr & filled_referents & filled_ss & filled_displ
    
    # for each in REF_TIERS:
    #     if each in full_set.keys():
    #         anomalies.update({each : [a for a in filled_referents.symmetric_difference(full_set[each])]})
    
    anomalies.update({('norm','ref-gr') : filled_referents.symmetric_difference(filled_refgr)})
    anomalies.update({('norm','ref-clause') : emb_cond1})
    anomalies.update({('norm','ref-fin') : emb_cond2})
    anomalies.update({('norm','ref-ss-emb-tag') : emb_cond3})
    anomalies.update({('norm','ref-ss-na-tag') : na_cond})
    anomalies.update({('norm','ref-displ-not-na') : displ_cond})
    
    for k,v in anomalies:
        d = {'file_name': os.path.basename(path),'speaker': k, 
                               'tier_name': v,
                               'token_numbers' : [a for a in anomalies[k,v]]}
        
        anomalous_data.append(d)
        
    write_to_csv(anomalous_data)

def _read_exb(path):
    """
    This function parses an EXMARaLDA XML file.
    :param path: Path to the xml file.
    :return: The XML object, a document map from annotations to
    """
    exb = ElementTree.parse(path)
    timeline = _timeline_map(exb.find(f'.//{_EXB_TAG_COMMON_TIMELINE}'))
    document_map = defaultdict(set)
    none_set_complete = defaultdict(set)
    for tier in exb.findall(f'.//{_EXB_TAG_TIER}'):
        anno_spec = (tier.attrib[_EXB_ATTR_SPEAKER], tier.attrib[_EXB_ATTR_CATEGORY])
        temp_set =set()
        merged_cells = set()
        
        if anno_spec in CONDITIONAL_TIERS and len(tier.findall('event'))==0:
            none_set_complete[anno_spec] = set(timeline.keys())
            document_map[anno_spec[1]] = set(timeline.keys())
        
        for event in tier:
            try:
                times = (event.attrib[_EXB_ATTR_START], event.attrib[_EXB_ATTR_END])
            except KeyError:
                _logger.error(f'Event has no or an incomplete time anchor and will be skipped.')
                continue
            anno_value = event.text
            if anno_value is not None:
                document_map[anno_spec + (anno_value,)].add(times)
            # to be able to look up where to find all annotations of a tier:
            document_map[anno_spec].add(times)  # by qname
            document_map[anno_spec[1]].add(times)  # by name
            
            if anno_spec in CONDITIONAL_TIERS:
                temp_set.update(get_timeline(times))
                
                if anno_value is None:
                    merged_cells.update(get_timeline(times))
                else:
                    none_set = set(timeline.keys()) - temp_set
                    none_set_complete[anno_spec] = none_set
                    none_set_complete[anno_spec].update(merged_cells)

            
    check_failed_conditions(path, document_map, none_set_complete)
            
    return exb, document_map, timeline, none_set_complete

def get_timeline(time_interval):
    start, end = time_interval[0][1:], time_interval[1][1:]
    timeline_range = list(range(int(start),int(end)))
    final_range=list()
    
    for item in timeline_range:
        final_range.append('T'+str(item))
        
    return final_range


def _timeline_map(timeline_element):
    """
    Convert an EXMARaLDA timeline to a str to float map.
    :param timeline_element: XML element representing the timeline.
    :return: Map from str to float
    """
    return {tli.attrib[_EXB_ATTR_ID]: float(tli.attrib[_EXB_ATTR_TIME])
            for tli in timeline_element.findall(f'.//{_EXB_TAG_TLI}')}


def _read_config(config_path):
    # TODO Here you can implement reading your own tagging configuration
    # this should map contexts to annotation names and values
    # a context is a collection of tuples of (tier_name, tier_value)
    # e. g. (('norm', 'pos_lang', 'NN'),) -> ('norm', 'pos', 'NOUN')
    # to allow for multiple conditions to be checked, contexts are tuples of tuples (tuples are hashable unlike lists)
    
    with open(config_path, 'r') as fd:
        rd = csv.reader(fd, delimiter='\t')
        config={}
        for row in rd:
            source_cond = tuple(map(str, row[0].split(':')))
            list_of_conditions=list()
            for cond in source_cond:
                oneCond = tuple(map(str, cond.split(',')))
                list_of_conditions.append(oneCond)
            target_cond = tuple(map(str, row[1].split(',')))
            
            config[tuple(list_of_conditions)] = (target_cond)
    
    #raise NotImplementedError
    return config


_JS_QNAME_SEPARATOR = '::'
_JS_NAME_SOURCE = 'source'
_JS_NAME_TARGET = 'target'
_JS_NAME_REGEX = 'regex'
_JS_NAME_MAP = 'map'


def _read_json(json_path):
    """
    This function reads a json configuration. This can only represent single conditions for a target annotation.
    :param json_path: Path to json file
    :return: Configuration
    """
    with open(json_path) as f:
        js = json.load(f)
    is_regex = js[_JS_NAME_REGEX]
    if is_regex:
        raise NotImplementedError
    source_speaker_name, source_tier_name = js[_JS_NAME_SOURCE].split(_JS_QNAME_SEPARATOR, 1)
    if _JS_QNAME_SEPARATOR in js[_JS_NAME_TARGET]:
        target_speaker_name, target_tier_name = js[_JS_NAME_TARGET].split(_JS_QNAME_SEPARATOR, 1)
    else:
        target_speaker_name = source_speaker_name
        target_tier_name = js[_JS_NAME_TARGET]
    value_map = js[_JS_NAME_MAP]
    config = {}
    for source_value, target_value in value_map.items():
        config[((source_speaker_name, source_tier_name, source_value),)] = \
            (target_speaker_name, target_tier_name, target_value)
    return config


if __name__ == '__main__':
    parser = ArgumentParser(description=_DESCRIPTION)
    parser.add_argument('file_or_dir', type=str, help='File or directory (searched recursively).')
    parser.add_argument('config_file', type=str, help='Path to config file.')
    parser.add_argument('--overwrite', action='store_true', help='Overwrite existing annotations.')
    parser.add_argument('--debug', action='store_true', help='Activate debug logging.')
    parser.add_argument('--log-file', action='store_true', help='Write log to file.')
    parser.add_argument('--dry', action='store_true', help='Dry run, do not write results.')
    args = parser.parse_args()
    # set up logger
    log_level = logging.DEBUG if args.debug else logging.INFO
    handler = logging.StreamHandler()
    handler.setLevel(log_level)
    _logger.setLevel(logging.DEBUG)
    _logger.addHandler(handler)
    if args.log_file:
        log_file_name = f'{os.path.splitext(os.path.basename(__file__))[0]}.log'
        file_handler = logging.FileHandler(log_file_name)
        file_handler.setLevel(logging.DEBUG)
        _logger.addHandler(file_handler)
    config_reader = _read_json if os.path.basename(args.config_file).endswith('tsv') else _read_config
    config = config_reader(args.config_file)
    for path in glob.iglob(f'{args.file_or_dir}**', recursive=True):
        if path.endswith('.exb'):
            map_to_tier(path, config, args.dry, args.overwrite)
